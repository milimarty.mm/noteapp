# Note App
App Dependencies:

- Hilt

- MVVM

- Navigation Component

- Live data

- Paging 3 

- Room

- Fab

- Persian Date

- Coroutine

- Material Design

- Kotlin

- Androidx

  

### App Arch

This is a single activity app that contains one activity and two fragments.
For storage and modification of items, room library is used. you can save folders inside other folders recursively.
On top of the data repository, which is room in our case, I used MVVM architecture.
And for dependency injection, I used hilt. 

Database UML :

![](./database_uml.png)







