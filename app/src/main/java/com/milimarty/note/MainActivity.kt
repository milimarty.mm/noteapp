package com.milimarty.note

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.widget.Toolbar
import com.milimarty.note.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private var _mBinding: ActivityMainBinding? = null
    private val mBinding
        get() = _mBinding!!


    lateinit var toolbar: Toolbar
    lateinit var onBackPressedClickListener: () -> Boolean

    private fun initToolbar() {
        setSupportActionBar(mBinding.baseToolbarLayout.baseToolbarLayout)
        toolbar = mBinding.baseToolbarLayout.baseToolbarLayout
    }


    private fun init() {
        initToolbar()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Light)
        super.onCreate(savedInstanceState)
        _mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        init()


    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main_page, menu)
        super.onCreateOptionsMenu(menu)
        return true
    }


    override fun onBackPressed() {
        if (::onBackPressedClickListener.isInitialized) {
            if (!onBackPressedClickListener.invoke())
                return
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        _mBinding = null
        super.onDestroy()
    }


}