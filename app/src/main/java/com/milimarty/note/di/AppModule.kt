package com.milimarty.note.di

import android.content.Context
import androidx.room.Room
import com.milimarty.note.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideYourDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        AppDatabase::class.java,
        "note_db"
    ).build()

    @Singleton
    @Provides
    fun provideTextNoteDao(db: AppDatabase) = db.getTextNoteDao()

    @Singleton
    @Provides
    fun provideNoteItemDao(db: AppDatabase) = db.getNoteItemDao()
}