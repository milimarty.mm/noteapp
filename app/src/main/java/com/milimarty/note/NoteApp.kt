package com.milimarty.note

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.milimarty.note.utils.Utils
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NoteApp : Application() {
    override fun onCreate() {
        /**
         * Change theme to DayLight
         */
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        super.onCreate()
    }
}