package com.milimarty.note

import androidx.appcompat.widget.Toolbar


fun Toolbar.onBackButtonClickListener(f: () -> Unit) {
        setNavigationIcon(R.drawable.ic_back_arrow)
        setNavigationOnClickListener {
            f.invoke()
        }
}

fun Toolbar.hideBackButton() {
   navigationIcon = null
}