package com.milimarty.note.customviews.dynamicmenu

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.SimpleAdapter
import androidx.appcompat.widget.ListPopupWindow
import androidx.core.content.ContextCompat
import com.milimarty.note.R
import com.milimarty.note.customviews.dynamicmenu.model.DynamicMenuModel
import com.milimarty.note.utils.Utils
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.forEach

private const val TITLE = "title"
private const val ICON = "icon"
private const val POPUP_MENU_WIDTH = 144f

/**
 * this class use to create dynamic popup menu
 */
class DynamicMenu(itemList: List<DynamicMenuModel>, var context: Context) {
    private var popupWindow: ListPopupWindow
    private val data: ArrayList<HashMap<String, Any>> = ArrayList()

    private lateinit var adapter: SimpleAdapter
    private lateinit var onItemClickListener: (Int) -> Unit


    init {
        /**
         * this func change DynamicMenuModel list to hashMap list
         */
        addPopupMenuItemList(itemList)

        popupWindow = ListPopupWindow(context).apply {
            setOnItemClickListener { parent, view, position, id ->
                if (::onItemClickListener.isInitialized) {
                    onItemClickListener.invoke((adapter.getItem(position) as HashMap<*, *>)[ICON] as Int)
                    dismiss()
                }

            }
            adapter = SimpleAdapter(
                context,
                data,
                R.layout.popup_menu_custom_layout,
                arrayOf(TITLE, ICON),
                intArrayOf(R.id.popup_menu_text_view, R.id.popup_menu_icon)
            )
            /**
             * adapter need to show item list
             */
            setAdapter(adapter)

            width = Utils.dpToPx(POPUP_MENU_WIDTH, context).toInt()

            setBackgroundDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.custom_popup_background
                )
            )
        }


    }

    fun setOnItemClickListener(onItemClickListener: (Int) -> Unit) {
        this.onItemClickListener = onItemClickListener

    }

    fun show(aView: View) {


        /**
         * initialize Popup Menu properties
         */
        popupWindow.apply {
            anchorView = aView
            show()
            listView?.let { listView ->
                listView.setSelector(R.drawable.ripple_effect_menu_item)
                listView.dividerHeight = 1000
                val sage = ColorDrawable(ContextCompat.getColor(context, R.color.black))
                popupWindow.listView!!.divider = sage
            }
        }
    }


    private fun addPopupMenuItemList(itemList: List<DynamicMenuModel>) {
        itemList.forEach { popupMenuItem ->
            data.add(hashMapOf(TITLE to popupMenuItem.title, ICON to popupMenuItem.iconId))
        }

    }


}