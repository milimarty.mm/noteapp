package com.milimarty.note.customviews.dynamicmenu.model

data class DynamicMenuModel(val title: String, val iconId:Int)
