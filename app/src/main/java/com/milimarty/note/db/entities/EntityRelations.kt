package com.milimarty.note.db.entities

import androidx.room.*



data class NoteItemAndParent(
    @Embedded val noteItemEntity: NoteItemEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "parentId"
    )
    val parent: NoteItemEntity
)

data class NoteItemAndTextNote(
    @Embedded var noteItemEntity: NoteItemEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "noteItem"
    )
    var textNoteEntity: TextNoteEntity
)





