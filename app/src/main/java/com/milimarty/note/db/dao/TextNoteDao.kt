package com.milimarty.note.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import com.milimarty.note.db.entities.NoteItemEntity
import com.milimarty.note.db.entities.TextNoteEntity
import java.util.*

@Dao
interface TextNoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTextNote(textNote: TextNoteEntity): Long

    @Delete
    fun deleteTextNote(textNote: TextNoteEntity)

    @Update
    fun updateTextNote(textNote: TextNoteEntity): Int

    @Query("SELECT * FROM text_note WHERE  noteItem= :noteItemId")
    fun getTextNoteByNoteItemId(noteItemId: Int): LiveData<TextNoteEntity>


}