package com.milimarty.note.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.milimarty.note.db.converters.DateConverter
import com.milimarty.note.db.dao.TextNoteDao
import com.milimarty.note.db.dao.NoteItemDao
import com.milimarty.note.db.entities.TextNoteEntity
import com.milimarty.note.db.entities.NoteItemEntity


@Database(
    entities = [NoteItemEntity::class, TextNoteEntity::class], // Tell the database the entries will hold data of this type
    version = 1
)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getNoteItemDao(): NoteItemDao
    abstract fun getTextNoteDao(): TextNoteDao
}