package com.milimarty.note.db.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.util.*


@Entity(
    foreignKeys = [ForeignKey(
        entity = NoteItemEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("parentId"),
        onDelete = ForeignKey.CASCADE
    )],
    tableName = "note_item"
)
@Parcelize
data class NoteItemEntity(
    @PrimaryKey(autoGenerate = true) var id: Int=0,
    var title: String,
    var count: Int,
    val isFolder: Boolean,
    var modify_date: Date = Date(),
    val parentId: Int?=null
):Parcelable