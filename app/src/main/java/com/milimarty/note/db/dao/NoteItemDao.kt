package com.milimarty.note.db.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.milimarty.note.db.entities.NoteItemEntity
import kotlinx.coroutines.selects.select

@Dao
interface NoteItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNoteItem(noteItem:NoteItemEntity):Long

    @Delete
    fun deleteNoteItem(noteItem:NoteItemEntity)

    @Update
    fun updateNoteItem(noteItem:NoteItemEntity)

    @Query("SELECT * FROM note_item WHERE parentId IS NULL ORDER BY modify_date DESC")
    fun getRootFolderNoteItemList():PagingSource<Int,NoteItemEntity>

    @Query("SELECT * FROM note_item WHERE parentId = :id ORDER BY modify_date DESC")
    fun getNoteItemListByFolderId(id:Int):PagingSource<Int,NoteItemEntity>

    @Query("SELECT * FROM note_item WHERE id = :id")
    fun getNoteItemByParentId(id:Int):NoteItemEntity

    @Query("DELETE FROM note_item WHERE id = :id")
    fun deleteByNoteItemId(id:Int)
    @Query( "SELECT count(*) FROM note_item WHERE parentId =:id and isFolder = 0")
    fun getFolderNoteItemsCount(id: Int):Long

}