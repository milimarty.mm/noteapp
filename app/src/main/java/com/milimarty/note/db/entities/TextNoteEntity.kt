package com.milimarty.note.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey


@Entity(
    tableName = "text_note",
    foreignKeys = [ForeignKey(
        entity = NoteItemEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("noteItem"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class TextNoteEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    var noteItem: Int,
    var noteText: String,
)

