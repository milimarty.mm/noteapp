package com.milimarty.note

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.milimarty.note.utils.G
import com.milimarty.note.utils.Utils

open class BaseActivity:AppCompatActivity() {
    override fun attachBaseContext(newBase: Context?) {
        /**
         * Change app language to farsi
         */
        super.attachBaseContext(Utils.changeAppLanguage(newBase!!, G.APP_LANGUAGE))
    }
}