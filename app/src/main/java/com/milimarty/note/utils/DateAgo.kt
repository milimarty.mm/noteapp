package com.milimarty.note.utils

import android.content.Context
import android.util.Log
import com.milimarty.note.R
import java.text.ParseException
import java.util.*
import java.util.concurrent.TimeUnit

object DateAgo {
    fun calculateLastSeen(pasTime: Date, context: Context): String {
        try {
            val nowTime = Date()
            val dateDiff: Long = nowTime.time - pasTime.time
            val second: Long = TimeUnit.MILLISECONDS.toSeconds(dateDiff)
            val minute: Long = TimeUnit.MILLISECONDS.toMinutes(dateDiff)
            val hour: Long = TimeUnit.MILLISECONDS.toHours(dateDiff)
            val day: Long = TimeUnit.MILLISECONDS.toDays(dateDiff)

            return when {
                second == 0L -> {
                    context.getString(R.string.now)
                }
                second < 60 -> {
                    lastSeenFixFormat(context.getString(R.string.second_ago), second)

                }
                minute < 60 -> {
                    lastSeenFixFormat(context.getString(R.string.minute_ago), minute)

                }
                hour < 24 -> {
                    lastSeenFixFormat(context.getString(R.string.hour_ago), hour)
                }
                day >= 7 -> {
                    when {
                        day > 360 -> {
                            lastSeenFixFormat(context.getString(R.string.year_ago), (day / 360))

                        }
                        day > 30 -> {
                            lastSeenFixFormat(context.getString(R.string.month_ago), (day / 30))
                        }
                        else -> {
                            lastSeenFixFormat(context.getString(R.string.week_ago), (day / 7))
                        }
                    }
                }
                day < 7 -> {
                    lastSeenFixFormat(context.getString(R.string.day_ago), day)

                }
                else -> ""
            }
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.d("DATE_AGO", e.message.toString())
            return ""
        }

    }

    fun lastSeenFixFormat(name: String, time: Long): String = String.format("%d $name", time)
}