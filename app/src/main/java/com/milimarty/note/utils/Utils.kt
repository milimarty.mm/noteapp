package com.milimarty.note.utils

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.TypedValue
import java.util.*


object Utils {
    fun dpToPx(dip: Float, context: Context) = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dip,
        context.resources.displayMetrics
    )

    /**
     * use to change app language
     */
    fun changeAppLanguage(context: Context, language: String): Context? {
        var _context = context
        val locale = Locale(language)
        Locale.setDefault(locale)
        val res = _context.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        _context = _context.createConfigurationContext(config)
        return _context
    }


    fun fromHtml(source: String?): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(source)
        }
    }


    fun toHtml(source: Spanned?): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.toHtml(source, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.toHtml(source)
        }
    }
}