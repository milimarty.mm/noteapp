package com.milimarty.note.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Typeface
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.CharacterStyle
import android.text.style.StyleSpan
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import com.milimarty.note.R
import com.milimarty.note.customviews.dynamicmenu.DynamicMenu
import com.milimarty.note.customviews.dynamicmenu.model.DynamicMenuModel


class NoteActionMenu(val editText: EditText, val aView: View) : ActionMode.Callback {
    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        mode?.
        menu?.clear()
        val context = editText.context
        var cs: CharacterStyle
        val start: Int = editText.selectionStart
        val end: Int = editText.selectionEnd
        val ssb = SpannableStringBuilder(editText.text)
        DynamicMenu(
            listOf(
                DynamicMenuModel(
                    title = context.getString(R.string.copy),
                    R.drawable.ic_copy
                ),
                DynamicMenuModel(
                    title = context.getString(R.string.paste),
                    R.drawable.ic_paste
                ),
                DynamicMenuModel(
                    title = context.getString(R.string.bold),
                    R.drawable.ic_bold
                ),
                DynamicMenuModel(
                    title = context.getString(R.string.italic),
                    R.drawable.ic_italic
                )
            ), context
        ).apply {
            editText.setSelection(start, end)
            setOnItemClickListener {
                when (it) {

                    R.drawable.ic_italic -> {
                        cs = StyleSpan(Typeface.ITALIC)
                        ssb.setSpan(cs, start, end, 1)
                        editText.text = ssb

                    }

                    R.drawable.ic_bold -> {
                        cs = StyleSpan(Typeface.BOLD)
                        ssb.setSpan(cs, start, end, 1)
                        editText.text = ssb
                    }

                    R.drawable.ic_copy -> {
                        val selectedText: String =
                            editText.text.toString().substring(start, end)
                        val clipboard =
                            context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        val clip: ClipData = ClipData.newPlainText("Note", selectedText)
                        clipboard.setPrimaryClip(clip)
                    }

                    R.drawable.ic_paste -> {

                        val clipboard =
                            context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                        val item = clipboard!!.primaryClip!!.getItemAt(0)
                        if (item.text != null) {
                            editText.text.delete(start ,end)
                            editText.text.insert(start, item.text)
                        }

                    }


                }
            }
        }.show(editText)

        return true

    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return false
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        return false
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
    }


}