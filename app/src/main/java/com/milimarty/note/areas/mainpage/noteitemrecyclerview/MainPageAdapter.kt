package com.milimarty.note.areas.mainpage.noteitemrecyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.milimarty.note.R
import com.milimarty.note.databinding.NoteItemCardViewBinding
import com.milimarty.note.db.entities.NoteItemEntity
import com.milimarty.note.utils.DateAgo


class MainPageAdapter(
    private val context: Context,
    private val noteItemClickListener: NoteItemClickListener
) :
    PagingDataAdapter<NoteItemEntity, MainPageAdapter.MainPageViewHolder>(diffUtils) {

    companion object {
        private val diffUtils = object : DiffUtil.ItemCallback<NoteItemEntity>() {
            override fun areItemsTheSame(
                oldItem: NoteItemEntity,
                newItem: NoteItemEntity
            ): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(
                oldItem: NoteItemEntity,
                newItem: NoteItemEntity
            ): Boolean {
                return oldItem == newItem
            }


        }
    }

    override fun onBindViewHolder(holder: MainPageViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainPageViewHolder {
        val binding =
            NoteItemCardViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainPageViewHolder(binding)
    }


    inner class MainPageViewHolder(private val binding: NoteItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.noteItemMoreImageButton.setOnClickListener { view ->
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    item?.let { noteItem ->
                        noteItemClickListener.onMoreClickListener(noteItem, view)
                    }
                }
            }


            binding.noteItemCardView.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    item?.let {
                        if (it.isFolder)
                            noteItemClickListener.onFolderClickListener(it)
                        else
                            noteItemClickListener.onNoteClickListener(it)

                    }
                }
            }

        }

        fun bind(item: NoteItemEntity) {
            /**
             * Setup view data
             */
            binding.apply {
                noteItemTitleTextView.text = item.title
                /**
                 * TODO SHAPE
                 */

                noteItemIconImageView.setBackgroundResource(
                    if (item.isFolder)
                        R.drawable.ic_circle_folder
                    else
                        R.drawable.ic_circle_note
                )

                noteItemDescriptionTextView
                    .text =
                    if (!item.isFolder)
                        DateAgo.calculateLastSeen(item.modify_date, context)
                    else
                        String.format(context.getString(R.string.item_count) ,item.count)
                    /**
                     * TODO Date , More
                     */

            }


        }
    }


}