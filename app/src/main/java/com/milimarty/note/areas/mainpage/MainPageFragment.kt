package com.milimarty.note.areas.mainpage

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.milimarty.note.MainActivity
import com.milimarty.note.R
import com.milimarty.note.areas.dialogs.DeleteItemDialog
import com.milimarty.note.areas.dialogs.FolderDialog
import com.milimarty.note.areas.mainpage.noteitemrecyclerview.MainPageAdapter
import com.milimarty.note.areas.mainpage.noteitemrecyclerview.MainPageLoadingAdapter
import com.milimarty.note.areas.mainpage.noteitemrecyclerview.NoteItemClickListener
import com.milimarty.note.areas.mainpage.viewmodel.MainPageViewModel
import com.milimarty.note.customviews.dynamicmenu.DynamicMenu
import com.milimarty.note.customviews.dynamicmenu.model.DynamicMenuModel
import com.milimarty.note.databinding.FragmentMainPageBinding
import com.milimarty.note.db.entities.NoteItemEntity
import com.milimarty.note.hideBackButton
import com.milimarty.note.onBackButtonClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*


private const val TAG = "MAIN_PAGE_FRAGMENT"

@AndroidEntryPoint
class MainPageFragment : Fragment(R.layout.fragment_main_page) {


    /**
     *  noteItem args
     */
    private val args by navArgs<MainPageFragmentArgs>()

    /**
     * Values
     */
    private val viewModel by viewModels<MainPageViewModel>()

    private var _mBinding: FragmentMainPageBinding? = null
    private val mBinding
        get() = _mBinding!!

    private lateinit var mainPageAdapter: MainPageAdapter


    /**
     * init dynamic menu
     */
    private val toolbarDynamicMenu: DynamicMenu by lazy {
        DynamicMenu(
            listOf(
                DynamicMenuModel(
                    requireContext().getString(R.string.delete_item),
                    R.drawable.ic_delete
                ),
                DynamicMenuModel(
                    requireContext().getString(R.string.edit_title),
                    R.drawable.ic_edit
                )

            ),
            requireContext()
        ).also { it ->
            it.setOnItemClickListener { itemId ->
                when (itemId) {
                    R.drawable.ic_delete -> {
                        DeleteItemDialog.create(requireContext()).apply {
                            show()
                            setOnDeleteButtonClickListener {
                                deleteFolder()
                            }
                        }
                    }

                    R.drawable.ic_edit -> {
                        FolderDialog.create(requireContext(), args.NoteItem?.title).apply {
                            show()
                            setOnAcceptClickListener { title ->
                                updateFolderTitle(title)
                            }
                        }
                    }
                }

            }
        }
    }

    private val mainActivity by lazy {
        (activity as MainActivity)
    }

    private val toolbar: Toolbar by lazy {
        mainActivity.toolbar
    }


    private fun setupRecyclerView() {
        mBinding.mainPageNoteItemRecyclerView.apply {
            adapter = mainPageAdapter
                .withLoadStateHeaderAndFooter(
                    header = MainPageLoadingAdapter(),
                    footer = MainPageLoadingAdapter()
                )
            layoutManager = LinearLayoutManager(context)

        }
    }

    private fun deleteFolder() {
        args.NoteItem?.let {
            viewModel.deleteNoteItem(it)
            findNavController().navigateUp()
        }

    }

    private fun updateFolderTitle(folderTitle: String) {
        args.NoteItem?.let {
            it.title = folderTitle
            viewModel.updateNoteItem(it)
            toolbar.title = folderTitle
        }

    }


    private fun init() {


        /**
         * init on button click listener
         */
        initListener()

        /**
         * init MainPage  Adapter
         */
        mainPageAdapter = MainPageAdapter(requireContext(), object : NoteItemClickListener {
            override fun onNoteClickListener(noteItem: NoteItemEntity) {
                val action = MainPageFragmentDirections.actionMainPageFragmentToNotePageFragment(
                    noteItem
                )
                findNavController().navigate(action)
            }

            override fun onFolderClickListener(noteItem: NoteItemEntity) {
                val action = MainPageFragmentDirections.actionMainPageFragmentSelf(noteItem)
                findNavController().navigate(action)
            }

            override fun onMoreClickListener(noteItem: NoteItemEntity, view: View) {
                val itemList = arrayListOf(
                    DynamicMenuModel(
                        requireContext().getString(R.string.delete_item),
                        R.drawable.ic_delete
                    )
                ).apply {
                    if (noteItem.isFolder)
                        add(
                            DynamicMenuModel(
                                requireContext().getString(R.string.edit_title),
                                R.drawable.ic_edit
                            )
                        )
                }
                DynamicMenu(
                    itemList,
                    requireContext()
                ).also {

                    it.setOnItemClickListener { itemId ->
                        when (itemId) {
                            R.drawable.ic_delete -> {
                                DeleteItemDialog.create(requireContext()).apply {
                                    show()
                                    setOnDeleteButtonClickListener {
                                        viewModel.deleteNoteItem(noteItem)
                                    }
                                }
                            }

                            R.drawable.ic_edit -> {
                                FolderDialog.create(requireContext(), noteItem.title).apply {
                                    show()
                                    setOnAcceptClickListener { title ->
                                        viewModel.updateNoteItem(noteItem.copy(title = title))
                                    }
                                }
                            }
                        }

                    }

                }.show(view)

            }

        })


        viewModel.noteItemList.observe(viewLifecycleOwner) {
            mainPageAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }


        /**
         * Set Toolbar properties
         */


        args.NoteItem?.let {
            toolbar.title = it.title
            toolbar.onBackButtonClickListener {
                findNavController().popBackStack()
            }
        } ?: kotlin.run {
            toolbar.title = requireActivity().getString(R.string.notes)
            toolbar.hideBackButton()
        }


        setupRecyclerView()
        setHasOptionsMenu(true)

        mainActivity.onBackPressedClickListener = { onBackPressed() }


//        FabMenu(requireContext(),mBinding)

//        /**
//         * Loading View
//         */
        initLoadState()

    }

    private fun initLoadState() {
        mainPageAdapter.addLoadStateListener {
            mBinding.apply {
                mainPageNoteItemRecyclerView.isVisible =
                    it.source.refresh is LoadState.Loading
                mainPageNoteItemRecyclerView.isVisible = it.source.refresh is LoadState.NotLoading
                recyclerViewIsEmptyRl.isVisible = it.source.refresh is LoadState.Error

                if (it.source.refresh is LoadState.NotLoading && it.append.endOfPaginationReached && mainPageAdapter.itemCount < 1) {
                    mainPageNoteItemRecyclerView.isVisible = false
                    recyclerViewIsEmptyRl.isVisible = true
                } else {
                    recyclerViewIsEmptyRl.isVisible = false

                }

            }
        }

    }

    private fun initListener() {
        mBinding.apply {
            fragmentMainCreateNoteFab.setOnClickListener {
                val action = MainPageFragmentDirections.actionMainPageFragmentToNotePageFragment(
                    args.NoteItem
                )
                fragmentMainFabMenu.close(true)
                findNavController().navigate(action)
            }

            fragmentMainCreateFolderFab.setOnClickListener {
                fragmentMainFabMenu.close(true)
                FolderDialog.create(requireContext()).apply {
                    show()
                    setOnAcceptClickListener {
                        viewModel.insertNoteItem(
                            NoteItemEntity(
                                title = it,
                                count = 0,
                                modify_date = Date(),
                                isFolder = true,
                                parentId = args.NoteItem?.id
                            )
                        )
                    }
                }


            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentMainPageBinding.bind(view)
        init()
        /**
         * Get ItemList from ViewModel
         */
        viewModel.getNoteItemListByFolderId(args.NoteItem?.id)
    }


    override fun onDestroy() {
        _mBinding = null
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        args.NoteItem ?: kotlin.run {
            for (i in 0 until menu.size()) menu.getItem(i).isVisible = false
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        toolbarDynamicMenu.show(toolbar)
        return super.onOptionsItemSelected(item)
    }

    private fun onBackPressed(): Boolean {
        return if (mBinding.fragmentMainFabMenu.isOpened) {
            mBinding.fragmentMainFabMenu.close(true)
            return false
        } else
            true

    }
}