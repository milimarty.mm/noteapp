package com.milimarty.note.areas.mainpage.repo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.milimarty.note.db.dao.NoteItemDao
import com.milimarty.note.db.entities.NoteItemEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject


const val PARE_PAGE: Int = 10

class MainPageRepository
@Inject constructor(private val noteItemDao: NoteItemDao) {


    fun getRootFolderNoteItemList() = Pager(
        config = PagingConfig(
            pageSize = PARE_PAGE,
            maxSize = 80,
            enablePlaceholders = false
        ),
        pagingSourceFactory = {
            noteItemDao.getRootFolderNoteItemList()
        }
    ).liveData

    fun getNoteItemListByFolderId(folderId: Int) = Pager(
        config = PagingConfig(
            pageSize = PARE_PAGE,
            maxSize = 80,
            enablePlaceholders = false
        ),
        pagingSourceFactory = {

            noteItemDao.getNoteItemListByFolderId(folderId)
        }
    ).liveData

    fun insertNoteItem(noteItem: NoteItemEntity) {
        CoroutineScope(IO).launch {
            noteItemDao.insertNoteItem(noteItem)
        }
    }


    fun deleteNoteItem(noteItem: NoteItemEntity) {
        CoroutineScope(Dispatchers.Default).launch {
            noteItemDao.deleteNoteItem(noteItem)
            noteItem.parentId?.let {
                updateParent(noteItem)
            }
        }
    }

    fun updateNoteItem(noteItem: NoteItemEntity) {
        CoroutineScope(Dispatchers.Default).launch {
            noteItemDao.updateNoteItem(noteItem)
        }
    }

    private fun updateParent(parent: NoteItemEntity): NoteItemEntity {

        return noteItemDao.getNoteItemByParentId(parent.parentId!!).apply {
            modify_date = Date()
            count = noteItemDao.getFolderNoteItemsCount(id).toInt()
            noteItemDao.updateNoteItem(this)
        }
    }
}