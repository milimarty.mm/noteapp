package com.milimarty.note.areas.mainpage.viewmodel

import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.milimarty.note.areas.mainpage.repo.MainPageRepository
import com.milimarty.note.db.entities.NoteItemEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainPageViewModel
@Inject constructor(private val repository: MainPageRepository) :
    ViewModel(), LifecycleObserver {

    private val searchTextLiveData = MutableLiveData<Int?>()
    val noteItemList = searchTextLiveData.switchMap {
        it?.let {
            repository.getNoteItemListByFolderId(it).cachedIn(viewModelScope)

        }?: kotlin.run {
            repository.getRootFolderNoteItemList().cachedIn(viewModelScope)
        }
    }

    fun getNoteItemListByFolderId(folderId:Int?=null){
        searchTextLiveData.value = folderId
    }

    fun insertNoteItem(noteItem: NoteItemEntity){
        repository.insertNoteItem(noteItem)
    }

    fun deleteNoteItem(noteItem: NoteItemEntity){
        repository.deleteNoteItem(noteItem)
    }

    fun updateNoteItem(noteItem: NoteItemEntity){
        repository.updateNoteItem(noteItem)
    }

}