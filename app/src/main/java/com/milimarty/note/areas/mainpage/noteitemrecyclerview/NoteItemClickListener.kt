package com.milimarty.note.areas.mainpage.noteitemrecyclerview

import android.view.View
import com.milimarty.note.db.entities.NoteItemEntity

interface NoteItemClickListener {
    fun onNoteClickListener(noteItem: NoteItemEntity)
    fun onFolderClickListener(noteItem: NoteItemEntity)
    fun onMoreClickListener(noteItem: NoteItemEntity,view: View)
}