package com.milimarty.note.areas.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.milimarty.note.R


class FolderDialog private constructor(context: Context, var folderTitle: String? = null) :
    Dialog(context) {

    companion object {
        fun create(context: Context, folderTitle: String? = null): FolderDialog {
            return FolderDialog(context, folderTitle).apply {
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
        }
    }


    private lateinit var acceptButton: Button
    private lateinit var cancelButton: Button
    private lateinit var titleTextView: TextView
    private lateinit var descriptionTextView: TextView
    private lateinit var editTitleEditText: EditText


    fun setOnAcceptClickListener(changeTitle: (String) -> Unit) {
        if (::acceptButton.isInitialized)
            acceptButton.setOnClickListener {
                editTitleEditText.text.toString().let {
                    if (it.isNotBlank()){
                        changeTitle.invoke(it)
                        dismiss()

                    }
                    else
                        Toast.makeText(context, R.string.fill_edit_title, Toast.LENGTH_LONG).show()
                }
            }
    }

    private fun initViews() {
        cancelButton = findViewById(R.id.dialog_folder_cancel_button)
        acceptButton = findViewById(R.id.dialog_folder_accept_button)
        titleTextView = findViewById(R.id.dialog_folder_title_text_view)
        descriptionTextView = findViewById(R.id.dialog_folder_descriptions_text_view)
        editTitleEditText = findViewById(R.id.dialog_folder_edit_title_edit_text)

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_folder)

        initViews()

        cancelButton.setOnClickListener { dismiss() }
        folderTitle?.let {
            titleTextView.setText(R.string.change_folder_title)
            descriptionTextView.visibility = View.GONE
            editTitleEditText.append(it)
            acceptButton.text = context.getString(R.string.save)
        } ?: kotlin.run {
            titleTextView.setText(R.string.new_folder)
            descriptionTextView.setText(R.string.set_new_folder_title)
            acceptButton.text = context.getString(R.string.create_new_folder)
        }

    }


}