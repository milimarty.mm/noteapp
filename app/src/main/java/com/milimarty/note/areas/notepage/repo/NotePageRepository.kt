package com.milimarty.note.areas.notepage.repo

import androidx.lifecycle.LiveData
import com.milimarty.note.db.dao.NoteItemDao
import com.milimarty.note.db.dao.TextNoteDao
import com.milimarty.note.db.entities.NoteItemAndTextNote
import com.milimarty.note.db.entities.NoteItemEntity
import com.milimarty.note.db.entities.TextNoteEntity
import kotlinx.coroutines.*
import java.util.*
import javax.inject.Inject

class NotePageRepository @Inject constructor(
    private val textNoteDao: TextNoteDao,
    private val noteItemDao: NoteItemDao
) {


    fun getTextNoteByNoteItemId(noteItemId: Int): LiveData<TextNoteEntity> {
        return textNoteDao.getTextNoteByNoteItemId(noteItemId)

    }

    fun deleteTextNote(noteItemAndTextNote: NoteItemAndTextNote) {
        val noteItem = noteItemAndTextNote.noteItemEntity
        val textNote = noteItemAndTextNote.textNoteEntity
        CoroutineScope(Dispatchers.IO).launch {
            textNoteDao.deleteTextNote(textNote)
            noteItemDao.deleteNoteItem(noteItem)
            noteItem.parentId?.let {
                updateParent(noteItem)
            }
        }


    }

    fun saveNoteItemAndTextNote(noteItemAndTextNote: NoteItemAndTextNote, isUpdate: Boolean) {
        val noteItem = noteItemAndTextNote.noteItemEntity
        val textNote = noteItemAndTextNote.textNoteEntity
        CoroutineScope(Dispatchers.IO).launch {
            if (isUpdate) {
                noteItem.apply {
                    modify_date = Date()
                    noteItemDao.updateNoteItem(this)

                }
            } else {
                noteItem.apply {
                    modify_date = Date()
                    id = noteItemDao.insertNoteItem(this).toInt()
                    textNote.noteItem = id


                }
            }
            noteItem.parentId?.let {
                updateParent(noteItem)
            }
            textNoteDao.insertTextNote(textNote)
        }

    }

    private fun updateParent(parent: NoteItemEntity): NoteItemEntity {

        return noteItemDao.getNoteItemByParentId(parent.parentId!!).apply {
            modify_date = Date()
            count = noteItemDao.getFolderNoteItemsCount(id).toInt()
            noteItemDao.updateNoteItem(this)
        }
    }
}