package com.milimarty.note.areas.notepage

import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.milimarty.note.MainActivity
import com.milimarty.note.R
import com.milimarty.note.areas.dialogs.DeleteItemDialog
import com.milimarty.note.areas.notepage.viewmodel.NotePageViewModel
import com.milimarty.note.customviews.dynamicmenu.DynamicMenu
import com.milimarty.note.customviews.dynamicmenu.model.DynamicMenuModel
import com.milimarty.note.databinding.FragmentNotePageBinding
import com.milimarty.note.db.entities.NoteItemAndTextNote
import com.milimarty.note.db.entities.NoteItemEntity
import com.milimarty.note.db.entities.TextNoteEntity
import com.milimarty.note.onBackButtonClickListener
import com.milimarty.note.utils.NoteActionMenu
import com.milimarty.note.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import saman.zamani.persiandate.PersianDate
import java.util.*


@AndroidEntryPoint
class NotePageFragment : Fragment(R.layout.fragment_note_page) {

    /**
     *  noteItem args
     */
    private val args by navArgs<NotePageFragmentArgs>()

    /**
     * Values
     */
    private val viewModel by viewModels<NotePageViewModel>()


    private var _mBinding: FragmentNotePageBinding? = null
    private val mBinding
        get() = _mBinding!!

    private lateinit var noteItem: NoteItemEntity

    private var textNote = TextNoteEntity(0, 0, "")
    private val noteItemAndTextNot by lazy {
        NoteItemAndTextNote(
            noteItemEntity = noteItem,
            textNoteEntity = textNote
        )
    }
    private var isUpdate: Boolean = false

    private val mainActivity by lazy {
        (activity as MainActivity)
    }

    private val toolbar: Toolbar by lazy {
        mainActivity.toolbar
    }


    private fun init() {


        /**
         * Set Toolbar properties
         */
        viewModel.getTextNoteByNoteItemId(0)
        toolbar.onBackButtonClickListener {
            if (mBinding.fragmentNoteTitleTextView.text.isEmpty() && mBinding.fragmentNoteNoteTextView.text.isNotEmpty()) {
                Toast.makeText(requireContext(), R.string.fill_edit_title, Toast.LENGTH_SHORT)
                    .show()
                return@onBackButtonClickListener
            } else
                save()
            findNavController().popBackStack()

        }
        args.noteItem?.let { item ->
            noteItem = if (item.isFolder)
                NoteItemEntity(0, "", 0, false, Date(), parentId = item.id)
            else {
                isUpdate = true
                item
            }

            mBinding.apply {
                fragmentNoteTitleTextView.append(noteItem.title)
                PersianDate(noteItem.modify_date).let {
                    fragmentNoteDateTextView.text =
                        String.format("%d  %s  %d", it.shDay, it.monthName(), it.shYear)

                }
                viewModel.getTextNoteByNoteItemId(noteItem.id).observe(viewLifecycleOwner) { note ->
                    if (note != null) {
                        textNote = note
                        fragmentNoteNoteTextView.append(Utils.fromHtml(textNote.noteText))
                    }

                }
            }
        } ?: run {
            noteItem = NoteItemEntity(0, "", 0, false, Date())
            PersianDate(Date()).let {
                mBinding.fragmentNoteDateTextView.text =
                    String.format("%d  %s  %d", it.shDay, it.monthName(), it.shYear)

            }
        }

        toolbar.title = ""
        setHasOptionsMenu(true)


        mBinding.fragmentNoteNoteTextView.customSelectionActionModeCallback =
            NoteActionMenu(mBinding.fragmentNoteNoteTextView, toolbar)


    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentNotePageBinding.bind(view)
        init()


    }


    private fun save() {
        if (noteItem.title != mBinding.fragmentNoteTitleTextView.text.toString()
            || textNote.noteText != Utils.toHtml(
                mBinding.fragmentNoteNoteTextView.text
            )
        ) {
            noteItem.title = mBinding.fragmentNoteTitleTextView.text.toString()
            textNote.noteText = Utils.toHtml(mBinding.fragmentNoteNoteTextView.text)

            viewModel.saveNoteItemAndTextNote(
                noteItemAndTextNot,
                isUpdate
            )
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        DynamicMenu(
            listOf(
                DynamicMenuModel(
                    requireContext().getString(R.string.delete_item),
                    R.drawable.ic_delete
                )

            ),
            requireContext()
        ).also {
            it.setOnItemClickListener { itemId ->
                when (itemId) {
                    R.drawable.ic_delete -> {
                        DeleteItemDialog.create(requireContext()).apply {
                            show()
                            setOnDeleteButtonClickListener {
                                args.noteItem?.let {
                                    viewModel.deleteTextNote(noteItemAndTextNot)
                                }
                                findNavController().navigateUp()

                            }
                        }
                    }
                }

            }
        }.show(toolbar)
        return super.onOptionsItemSelected(item)
    }

}
