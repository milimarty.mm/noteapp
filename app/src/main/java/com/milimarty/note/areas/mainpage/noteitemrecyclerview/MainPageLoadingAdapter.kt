package com.milimarty.note.areas.mainpage.noteitemrecyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.milimarty.note.databinding.NoteRecyclerViewLoadingFooterBinding

class MainPageLoadingAdapter :
    LoadStateAdapter<MainPageLoadingAdapter.LoadStateViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        val binding =
            NoteRecyclerViewLoadingFooterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoadStateViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)

    }


    inner class LoadStateViewHolder(private val binding: NoteRecyclerViewLoadingFooterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(loadState: LoadState) {
            binding.apply {
                noteRecyclerViewLoadingStateLoadingBar.isVisible = loadState is LoadState.Loading

            }
        }

    }


}