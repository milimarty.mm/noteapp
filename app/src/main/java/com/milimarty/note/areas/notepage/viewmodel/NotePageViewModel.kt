package com.milimarty.note.areas.notepage.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.milimarty.note.areas.notepage.repo.NotePageRepository
import com.milimarty.note.db.entities.NoteItemAndTextNote
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CompletableJob
import javax.inject.Inject

@HiltViewModel
class NotePageViewModel @Inject constructor(private val notePageRepository: NotePageRepository) :
    ViewModel(), LifecycleObserver {


    fun getTextNoteByNoteItemId(noteItemId: Int) =
        notePageRepository.getTextNoteByNoteItemId(noteItemId)


    fun deleteTextNote(noteItemAndTextNote: NoteItemAndTextNote) {
        notePageRepository.deleteTextNote(noteItemAndTextNote)
    }

    fun saveNoteItemAndTextNote(noteItemAndTextNote: NoteItemAndTextNote,isUpdate: Boolean) {
        notePageRepository.saveNoteItemAndTextNote(noteItemAndTextNote, isUpdate)
    }


}