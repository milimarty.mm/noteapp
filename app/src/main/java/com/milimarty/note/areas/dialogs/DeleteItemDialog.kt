package com.milimarty.note.areas.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.milimarty.note.R


class DeleteItemDialog private constructor(context: Context, var isFolder: Boolean = false) :
    Dialog(context) {

    companion object {
        fun create(context: Context, isFolder: Boolean = false): DeleteItemDialog {
            return DeleteItemDialog(context, isFolder).apply {
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
        }
    }


    private lateinit var deleteButton: Button
    private lateinit var cancelButton: Button
    private lateinit var titleTextView: TextView
    private lateinit var descriptionTextView: TextView

    fun setOnDeleteButtonClickListener(clickListener: () -> Unit) {
        if (::deleteButton.isInitialized)
            deleteButton.setOnClickListener {
                clickListener.invoke()
                dismiss()
            }
    }

    private fun initViews() {
        cancelButton = findViewById(R.id.dialog_delete_item_cancel_button)
        deleteButton = findViewById(R.id.dialog_delete_item_delete_button)
        titleTextView = findViewById(R.id.dialog_delete_item_title_text_view)
        descriptionTextView = findViewById(R.id.dialog_delete_item_descriptions_text_view)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_delete_item)

        initViews()

        cancelButton.setOnClickListener { dismiss() }
        titleTextView.setText(R.string.delete_item)
        descriptionTextView
            .setText(
                if (isFolder)
                    R.string.dialog_delete_note_description
                else
                    R.string.dialog_delete_folder_description
            )
    }


}